Title: Graphics
Published: 2019-01-07 18:24
...

I've been spending some time learning how to do a bit of graphics in rust. So far it sucks. A lot of this, I think, is simply due to my complete inexperience with graphical UI programming outside of web browsers. I've split my process of doing my first GUI program in rust into steps below.


### Step 1: Pick a library

Since rust is fairly young for a programming language, picking libraries can understandably be a bit of a minefield. While there's a whole lot of exciting codebases to try out, many end up abandoned or add breaking changes faster than your code can compile.


At the outset I figured it would boil down to SDL or OpenGL -- both of which have libraries in rust! However, I was keen to try and use the rustiest libraries possible.


After glossing over some blog posts and github pages I settled on Gfx-rs. It seems pretty professionally maintained, looks extremely popular, and is already at least somewhat involved with some of the biggest rust projects I know of (Amethyst and Servo). Great! While all I need to do is draw some pixels, surely learning a bit about computer graphics would also be beneficial -- after all, this is supposed to be a learning project.


I was admittedly also drawn to Gfx-rs's claim to be very *low-level*. I'm trying to get a little better at programming by tackling lower-level concepts outside my usual comfort zone, so the words "low-level" in the README immedately caught my attention. So, throwing the other libraries to the wind, I buckled down to get started.


### Step 2: Look at some examples

I cracked open the simplest available gfx-rs demo available at the [time of writing](https://github.com/gfx-rs/gfx/tree/c6fbeadfa973c5124ddaa2be094a73fd966c7997/examples) called "Quad", and was faced with **770 lines of code** to draw the gfx-rs logo on a square.


*Fuck.*


Granted, this is only the second bit of rust I've ever looked at, and my first time trying my hand at graphics. So, I peeled back my sleeves tried to understand what was going on. Cross referencing some other examples and doing some *very* light reading on openGL / Vulkan didn't help too much, as what I could find felt extremely technical. In hindsight, I need to stop being intimidated by technical documentation and just **read** the damn stuff -- I am, after all, attempting to start learning more complicated concepts.


Instead I decided to do what any beginner would do:


#### Step 2.1: Look for some easier examples...

Eventually after poking around on google I found [this excellent tutorial](https://suhr.github.io/gsgt/) walking through the process of making squares. *Sweet!* In fact, this tutorial was probably going to get me 90% to where I needed. Since I'm ultimately going to be working in 2D, putting a texture on a square in OpenGL is *exactly* what I need to be able to do.


I followed along happily with the tutorial until I hit my first real snag: my results weren't consistent with the tutorial. Every beginners' nightmare. Even copying and pasting the code directly, the squares I was drawing *weren't where they should be*. I figured it was some kind of HiDPI issue which would be easily resolved at the time so I just tried hacking at it for what ended up being far too long...


#### Step 2.2: Upgrade crates after 4 days of infuriating failure

So it turns out I just needed to update all the crates. Since I hate myself, I'm doing all of this experimentation on a Pixelbook in the Linux environment provided (crostini). The issue I was having *was* HiDPI-replated, and got resolved pretty much automatically in a newer version of glutin.


Now there was a new issue -- almost all my updated crates had breaking changes. I dug through the documentation for glutin, winit, etc. until everything was working again. Suddenly my code was looking fairly different from the tutorial... This wasn't going as smoothly as I had hoped.


#### Step 2.3: Find the *perfect* example

Then I found my holy grail. [This git repo](https://github.com/unpatched/internet-broadcasting-service), using similar concepts to the tutorial above manages to stream video to a texture on a square while also saving/encoding it. (At least I think that's what it does, I never actually tried to run the code.)


The code here, combined with the tutorial from step 2.2, gave me everything I needed to conceptually build what I wanted: a square with an updatable texture which would scale correctly in its window. With all my needs assembed it was time to: 


### Step 3: Get Rusty

Different programming langauges have a different *feel* to them. Not just the syntax and packagjjje management styles, or compiled vs interpreted. I primarily code in python and javascript, and switching between the two is a real paradigm shift somtimes, besides both being very loosely typed languages.


Writing code in javascript, to me, always feels **dirty**. Not dirty in a bad way, but dirty like playing in the sand as a kid -- its sloppy cheap fun. That isn't to say, of course, it isn't possible to build fantastic, well-regimented codebases in the language, but no matter what you do you'll end up with sand in your shoes.


When I write in javascript, I'm going to end up with a lot of little codepens and repl.it tabs open, as well as end up on a lot of Medium articles and blog posts (or, of course, stackoverflow) if I happen to be using some new methodology or tool.


Javascript is fun, but frustrating. You look away for three seconds and suddenly jQuery is basically obsolete, or React changed some core thing or es12 is out and now you can just make a full stateful boostrap25 website by typing `<\wB~... => ()`. Javascript's saving grace is that its so damn easy to learn and use that it doesn't really matter that things change all the time.


Writing code in python, on the other hand, is like playing with legos. The blocks fit together nicely and are easy to experiment with. Sometimes what you're building even has those giant kit-specific pieces that kinda takes care of a whole chunk of the project at once. I *like* writing code in python, I do it for 10 hours a day most weeks -- the language has always felt intuitive to me, just like how you can give anybody legos and they'll start building something.


When I'm in python mode I'm usually looking at some documentation or other on readthedocs.io, and have two or thee tabs on stackoverflow open if I've hit a new snag. If I need to do something really nasty I'll crack open the source code of one of the installed packagaes and do some clever subclassing. Overall, though, and despite the Python 2 vs 3 fun, python always feels calm and steady to me.


Of course, python's big issue is the things that aren't easy to deal with in python. Tracking down errors is *usually* easy (of course your program's already broken in runtime if you're tracking down errors), but sometimes something happens in a weird place, and you're *stuck* -- forced to do black magic and guesswork to pinpoint the issue. Python also makes certain things *too easy*, occasionally resulting in bad code which will ultimately cause things like memory leaks -- sometimes in ways the developer doesn't even understand.


In comparison with my two daily drivers, so far working with Rust has been more like trying to build a shed. Suddenly I can't just click things together like I could in lego, and frankly, I don't know how all the tools works yet. Since there's not as many people *using* rust as my other two languages, I can't just beg around on stackoverflow for quick answers, either.


Rust's automatic documentation tools in cargo are great -- but I've discovered the resulting docs are often fairly sparse. You (hopefully) get a couple of sparse examples using approximately 1% of the available structs, traits, and functions in the library. The rest is links to the defnitions of the other 99%. Most of my time was spent in the actual source code of libraries trying to puzzle out how the hell to use a given thing.


The saving grace here is rust's compiler. Dear lord this badboy feels slick out of the box! I'd be lying if I claimed to be anywhere near comfortable with the langauge yet, but if using rust is like building a shed, then the compiler is like a patient ex-contractor father helping you do it. Yeah he can't use the tools himself anymore, but he's determined to make sure you do things the right way.


At the end of the day, I got my shed built, and since I was forced to read manuals and blueprints, and had the compiler explaining my mistakes along the way, I think I could build the next shed a lot more comfortably. The road in rust is definitely more bumpy at the moment, but I imagine one day, if it can get the kind of massive community that other larger languages have (and better example coverage, goddamnit), it will probably be the most comfortable compiled langauge to work in.


### Step 4: Get your legs blown off

Remember the minefield I mentioned all the way back in Step 1? Well I fucking stepped on one. There was a reason the tutorial and perfect example were so much more concise and readible than the "Quad" example on the gfx-rs repository -- because gfx-rs has now been replaced by gfx-hal, a lower-level library that abstracts graphics programming so it'll work accross different hardwares. So while the *old* version of gfx-rs I've begun to learn is still techincally being developed, it's **old news.**


## The End Result

Despite being in an outdated version of gfx-rs, I've now got a fully functioning "canvas", if you will. A simple resizable aspect-ratio-maintaining rectangle struct with an update function allowing for the texture to be updated.


[You can check out the repo here.](https://gitlab.com/as-i-do/graphics-1)


![We have graphics!](resources/window.gif "We have graphics!")


While I'm still fairly bewildered by half of the stuff in the codebase, I did learn quite a bit about using rust. Fortunately for me this is about as far as I'll need to go in the GL department for a while, as I'm going to be heavily focused on basic 2D graphics for some time to come.


On that note, I'm debating whether or not to bother learning the **new** gfx-rs, as I can't leverage Vulkan or Metal with my current hardware, and the old version does seem to still be maintained. Since I'm not too interested in investing weeks learning graphics-domain stuff just to draw shitty low-rez pixel displays, I'm leaning towards moving on with this.


Either way, the next **big** step for me will be to integrate this graphical window into my existing Conway's Game of Life code. Progress is slow, but progress it is!
