Title: Project Start
Published: 2018-12-07 21:27
...
Recently I've found myself wanting to become a better programmer. I work in a space where I've been considered a "good" programmer despite having no formal training, and after a few years the harsh reality is beginning to creep in that I have built a castle of shit that me and my users must now live in.


I've been spending a lot of time lately refactoring thing with better practices, clearer implementatons, and more internal documentation -- but I often find myself slipping back into my shit-building ways, especially when needing to hot-patch a particularly messy piece of code.


After some introspection, I've decided one of my problems is that I'm just *too comfortable* with my daily driver languages. I taught myself how to code, and did it with python and javascript; arguably some of the easiest languages to pick up due to their wide usage and simple, lax syntaxes. It feels to me that these languages are my lik native tongues -- I never seem to have any trouble accomplishing or expressing anything I want to with them, however much like spoken languages just because you can speak it comfortably doesn't meen you're well articulated. 


And so here I am, brought up in a brothel and now trying to run a professional hotel, but I keep fucking the patrons. What I need to do is spend some time outside the hospitality industry -- I need to get away from my comfort-zone languages and projects. No python, no javascript, no web-apps. With luck, the process will endow me with new insights and abilities which will be transferable to my professional work. While I'll still be teaching myself, I'm going to try to do it properly this time, read some textbooks, and take my time to learn the *right way* to do things.


I've settled on Rust as my new language of choice. The 2018 version just dropped, and the language itself has been garnering a lot of positive attention. As for a project, I am proposing for myself a series of increasingly complex projects which should culminate in a Gameboy emulator *and* a homebrew game for said emulator (and, presumably, other emulators). My thinking is that while building the emulator will be an excellent and challenging project, building a game for it will truly test my knowledge of how the whole thing works, as well as be far more rewarding of a final product. 


This is a project entirely outside of my wheelhouse -- I have never worked with low-level code beyond arduino tutorials or emulated a microprocessor. I have never made a videogame before, either, despite it being a personal life-long goal. I'm likely to end up on many tangential sub-projects over the duration of this project, so I've started this blog to document the process and assist in keeping me focused.


With luck and perserverence, and likely quite a few trips to stackoverflow, I'm confident I can get this project done, and fortify my own abilities as a programmer along the way. 
