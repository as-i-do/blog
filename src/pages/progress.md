Title: Progress
...
Here I will account progress towards the completion of projects, as well as track cancelled or failed subprojects.

#### Project: Make a game

- [ ] **Learn some Rust**
    - [ ] **Read the Rust Programming Language Textbook**
    - [ ] **Practice Rust**
        - [ ] **Conway's Game of Life**
            - [x] Terminal
            - [ ] Gui
            - [ ] Web?
- [ ] Build a Chip-8 Emulator
- [ ] Build a game for Chip-8 Emulator
- [ ] Build a Gameboy(Color) Emulator
- [ ] Build a game for Gameboy(Color) Emulator
