Title: About
...
This is my personal project blog, primarily for tracking my progress on a rust-based from-scratch emulator and game project.


Professionally, I develop internal web-based tools for a Canadian university.


The site is currently hosted on gitlab pages using my own shoestring [static-site generator script](https://gitlab.com/mirkovu/compile.py).
